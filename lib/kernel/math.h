#ifndef __LIB_KERNEL_MATH_H
#define __LIB_KERNEL_MATH_H

inline int max(const int a, const int b);

inline int abs(const int a);

#endif
