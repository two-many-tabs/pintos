#include "lib/kernel/math.h"

/* Returns the greater integer of A and B. */
inline int max(const int a, const int b)
{
	return (a >= b) ? a : b;
}

/* Returns the absulute number of A. */
inline int abs(const int a)
{
	return (a < 0) ? -a : a;
}
