#ifndef VM_SPT_H
#define VM_SPT_H

#include <stdbool.h>
#include <stdlib.h>

#include "threads/interrupt.h"
#include "filesys/file.h"
#include "filesys/off_t.h"
#include "userprog/process.h"
#include "userprog/pagedir.h"
#include "lib/kernel/hash.h"

/* Represents an entry in the supplemental page table.
   One entry corresponds to exactly one file page. */
struct spte {
	struct hash_elem hash_elem; /* Used by hash table */
	uint8_t *vaddr;             /* Virtual address of the entry. (is page aligned) */
	struct file *file;          /* File to which the page is mapped */
	off_t ofs;                  /* Offset where the page mapping starts in the file */
	size_t size;                /* How far (starting at OFS) the mapping reaches */
};

bool spt_set_entry(struct process *p, struct spte *entry);

struct spte *spt_get_entry(struct process *p, void *vaddr);

bool spt_remove_entry(struct process *p, void *vaddr);

unsigned vaddr_hash_func(const struct hash_elem *e, void *aux UNUSED);

bool vaddr_hash_less_func(const struct hash_elem *a,
		const struct hash_elem *b, void *aux UNUSED);

void free_spt_entry(struct hash_elem *e, void *aux UNUSED);

#endif				/* vm/spt.h */
