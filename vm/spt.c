#include <stdio.h>

#include "vm/spt.h"
#include "vm/vm.h"
#include "threads/malloc.h"
#include "threads/palloc.h"

/* Insert a new entry ENTRY to the supplemental page table of process P.

   Internally, a hash table is used to manage insertions and lookups. The
   function will handle permanent allocation of the entry itself.

   If insertion is not possible, FALSE is returned. */
bool spt_set_entry(struct process *p, struct spte *entry)
{
	ASSERT(p != NULL);
	ASSERT(entry != NULL);
	ASSERT(is_page_aligned(entry->vaddr));

	struct spte *new_entry = malloc(sizeof(struct spte));
	new_entry->vaddr = entry->vaddr;
	new_entry->file = entry->file;
	new_entry->ofs = entry->ofs;
	new_entry->size = entry->size;

	struct hash_elem *e = hash_insert(&p->spt, &new_entry->hash_elem);

	return e == NULL;
}

/* Return the supplemental page table entry for address VADDR of process P.
   Returns NULL if the entry could not be found. */
struct spte *spt_get_entry(struct process *p, void *vaddr)
{
	ASSERT(p != NULL);
	ASSERT(vaddr != NULL);
	ASSERT(is_page_aligned(vaddr));

	struct spte spte;
	struct spte *result;
	struct hash_elem *e;

	spte.vaddr = vaddr;

	e = hash_find(&p->spt, &spte.hash_elem);

	if (e == NULL)
		return NULL;

	result = hash_entry(e, struct spte, hash_elem);

	ASSERT(result != NULL);

	return result;
}

/* Remove the supplemental page table entry for address VADDR of process P.
   Returns TRUE if the entry was found and deleted, FALSE otherwise. */
bool spt_remove_entry(struct process *p, void *vaddr) {
	ASSERT(p != NULL);
	ASSERT(vaddr != NULL);
	ASSERT(is_page_aligned(vaddr));

	struct spte spte;
	spte.vaddr = vaddr;

	struct hash_elem *e = hash_delete(&p->spt, &spte.hash_elem);

	return e != NULL;
}

/* Hash function used for the hash table to manage supplemental page table
   entries. Returns the hash of the virtual address associated with E. */
unsigned vaddr_hash_func(const struct hash_elem *e, void *aux UNUSED)
{
	struct spte *entry = hash_entry(e, struct spte, hash_elem);

	unsigned hash = hash_bytes(&entry->vaddr, sizeof(entry->vaddr));

	return hash;
}

/* Helper function used to compare the hashes of two virtual addresses in
   order to manage supplemental page table entries. Returns TRUE if A is less
   than B, FALSE otherwise. */
bool vaddr_hash_less_func(const struct hash_elem *a,
		const struct hash_elem *b, void *aux UNUSED)
{
	struct spte *entry_a = hash_entry(a, struct spte, hash_elem);
	struct spte *entry_b = hash_entry(b, struct spte, hash_elem);

	return entry_a->vaddr < entry_b->vaddr;
}

/* Helper function to free all hash table members if the process dies. */
void free_spt_entry(struct hash_elem *e, void *aux UNUSED)
{
	struct spte *entry = hash_entry(e, struct spte, hash_elem);
	free(entry);
}
