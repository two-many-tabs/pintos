#include <inttypes.h>
#include <stdio.h>
#include <string.h>

#include "threads/thread.h"
#include "vm/spt.h"
#include "vm/vm.h"
#include "threads/vaddr.h"
#include "threads/palloc.h"
#include "threads/pte.h"
#include "threads/malloc.h"

/* Handles a user page fault by determining if it was a stack access or not and
 * allocating a new page accordingly. Returns o on success and 1 otherwise. */
int handle_user_page_fault(struct thread *t, void *fault_addr, uint8_t *sp) {
	int success = 0;
	void *upage = (void *) pg_round_down(fault_addr);

	if (is_stack_access(t, fault_addr, sp))
		success = handle_stack_growth(upage);
	else
		success = handle_lazy_loading(upage, t, fault_addr);

	return success;
}

/* This is a helper function to check if some memory access to address
   FAULT_ADDR was intended to be an access to the program stack. If the page
   is flagged as reserved for any other use case already, FALSE is returned.
   Next, it is checked whether the stack size limit MAX_STACK_SIZE would be
   exceeded by the access to that address. Futhermore, accesses to the stack
   are restricted to occur only in a certain range MAX_STACK_OFFSET to the
   current program stack pointer. */
bool is_stack_access(struct thread *t, void *fault_addr, uint8_t *sp)
{
	uint32_t *pte = lookup_page(t->pagedir, fault_addr, true);
	if (pte == NULL)
		/* Actually, FALSE is not the right answer, nor is TRUE. */
		return false;

	struct page_flags flags;
	flags = parse_page_flags(*pte);

	if (flags.avl == AVL_STACK_PAGE)
		/* Page was probably swapped out. */
		return true;

	if (flags.avl != AVL_NOT_SET)
		return false;

	if (fault_addr + MAX_STACK_SIZE < PHYS_BASE)
		/* The stack cannot grow largern than MAX_STACK_SIZE. */
		return false;

	// TODO discussion talk: why does pt-grow-pusha generate a diff of 4 only,
	// although it's supposed to test for a diff of 32?
	int diff = (void *) sp - fault_addr;

	return (diff <= MAX_STACK_OFFSET);
}

/* This is a helper function to lazily load the file page at address
   FAULT_ADDR. To do so, the supplemental page table is queried. The found
   entry is used to look up the data for the page to be filled. Any left space
   is filled with zero bytes. */
uint8_t *lazy_load_file_page(struct process *p, void *upage)
{
	ASSERT(p != NULL);
	ASSERT(upage != NULL);
	ASSERT(is_page_aligned(upage));

	/* Get a page of memory. */
	uint8_t *kpage = palloc_get_page(PAL_USER);
	if (kpage == NULL)
		return NULL;

	struct spte *entry = spt_get_entry(p, upage);
	ASSERT(entry != NULL);

	/* Coarse grained filesystem lock for loading */
	lock_acquire(&filesys_lock);

	/* Fill in executable source content. */
	file_seek(entry->file, entry->ofs);

	if (file_read(entry->file, kpage, entry->size) != (off_t) entry->size) {
		/* We expected to read a whole page from the file. */
		palloc_free_page(kpage);
		lock_release(&filesys_lock);
		return NULL;
	}

	lock_release(&filesys_lock);

	/* Fill left space with zero. */
	memset(kpage + entry->size, 0, PGSIZE - entry->size);

	return kpage;
}

/* This is a helper function to provide stack growth functionality. It simply
   creates a new all-zero page flagged present and writable. It returns 0 on
   success and 1 otherwise. */
int handle_stack_growth(uint8_t *upage)
{
	/* Get a page of memory. */
	uint8_t *kpage = palloc_get_page(PAL_USER | PAL_ZERO);
	if (kpage == NULL)
		return 1;

	struct page_flags flags = {
		.present = true,
		.writable = true,
		.avl = AVL_STACK_PAGE
	};

	/* Add the page to the process's address space. */
	if (!install_page(upage, kpage, flags)) {
		palloc_free_page(kpage);
		return 1;
	}

	return 0;
}

/* This is a helper function to lazily load the page at UPAGE for thread T.
   First, we look for an entry of FAULT_ADDR in the page table. If it does not
   exist, the process is killed. Otherwise, we decide whether it's an all-zero
   page or not based on the page table entry. If it is, an according page is
   returned. In the case of a file page, another helper function is called. It
   returns 0 on success and 1 otherwise. */
int handle_lazy_loading(uint8_t *upage, struct thread *t, void *fault_addr)
{
	uint32_t *pte = lookup_page(t->pagedir, fault_addr, false);
	bool entry_exists = (pte != NULL);

	if (entry_exists) {
		uint8_t *kpage;

		struct page_flags flags;
		flags = parse_page_flags(*pte);
		flags.present = true;

		switch (flags.avl) {
		case AVL_FILE_PAGE:
			kpage = lazy_load_file_page(t->process, upage);
			break;
		case AVL_ZERO_PAGE:
			kpage = palloc_get_page(PAL_USER | PAL_ZERO);
			break;
		default:
			/* In this case, avl is set to AVL_NOT_SET and the stack heurisitcs
			 * determined it's not a stack access either. Conclusionally, the
			 * process tried to access memory it shouldn't. */
			thread_exit();
		}

		if (kpage == NULL)
			return 1;

		/* Add the page to the process's address space. */
		if (!install_page(upage, kpage, flags)) {
			palloc_free_page(kpage);
			// TODO discussion talk: why does kill not work here? (pt-write-code2)
			thread_exit();
		}
	} else {
		/* user process access violation */
		thread_exit();
	}

	return 0;
}

/* Checks if there are enough pages that haven't been reserved yet to house
   a data segment of length SIZE starting ad VADDR. Returns TRUE if there is
   enough space and FALSE otherwise. */
bool is_free_segment(void *vaddr, size_t size) {
	ASSERT(is_page_aligned(vaddr));
	ASSERT(size > 0);

	struct thread *t = thread_current();

	while (size > 0) {
		if (pagedir_is_page_reserved(t->pagedir, vaddr))
			return false;

		vaddr += PGSIZE;

		if (size > PGSIZE)
			size -= PGSIZE;
		else
			size = 0;
	}

	return true;
}

/* Prepares a segment starting at offset OFS in FILE at address UPAGE. In
   total, READ_BYTES + ZERO_BYTES bytes of virtual memory are reserved in the
   supplemental page table, as follows:

        - READ_BYTES bytes at UPAGE must be read from FILE starting at offset
          OFS.

        - ZERO_BYTES bytes at UPAGE + READ_BYTES must be zeroed.

   Later, when a page prepared is lazily loaded, all needed information can be
   queried using the supplemental page table.

   This function will not check if the segment is already in use. Call
   is_free_segment() if you are unsure about the state of the segment you are
   going to prepare.

   The pages handled by this function must be writable by the user process if
   WRITABLE is true, read-only otherwise.

   Return true if successful, false if a memory allocation error or disk
   read error occurs. */
bool prepare_segment(struct file *file, off_t ofs, uint8_t *upage,
		uint32_t read_bytes, uint32_t zero_bytes, bool writable)
{
	ASSERT((read_bytes + zero_bytes) % PGSIZE == 0);
	ASSERT(pg_ofs(upage) == 0);
	ASSERT(ofs % PGSIZE == 0);

	while (read_bytes > 0 || zero_bytes > 0) {
		/* Calculate how to fill this page. We will read PAGE_READ_BYTES bytes
		   from FILE and zero the final PAGE_ZERO_BYTES bytes. */
		size_t page_read_bytes =
		    read_bytes < PGSIZE ? read_bytes : PGSIZE;
		size_t page_zero_bytes = PGSIZE - page_read_bytes;

		struct page_flags flags = {
			.writable = writable,
			.present = false
		};

		if (page_read_bytes == 0) {
			flags.avl = AVL_ZERO_PAGE;
		} else {
			flags.avl = AVL_FILE_PAGE;

			struct spte entry = {
				.vaddr = upage,
				.file = file,
				.ofs = ofs,
				.size = page_read_bytes
			};

			bool success = spt_set_entry(process_current(), &entry);
			/* The caller has to check if the segment is free. */
			ASSERT(success);
		}

		/* We need to pass a kernel virtual address. */
		if (!install_page(upage, PHYS_BASE, flags))
			return false;

		/* Advance. */
		read_bytes -= page_read_bytes;
		zero_bytes -= page_zero_bytes;
		upage += PGSIZE;
		ofs += PGSIZE;
	}

	return true;
}

/* Goes through a segment that was previously initialized by prepare_segment.
   If a page of the segment is a file page and has already been loaded and it
   is written back to the file and the page is freed. Otherwise the page is
   simply uninstalled. */
bool free_segment(void *upage, size_t size)
{
	ASSERT(is_page_aligned(upage));
	ASSERT(size > 0);

	struct thread *t = thread_current();
	void *kpage;

	/* Set all pages belonging to the segment to not present. */
	while (true) {
		struct spte *entry = spt_get_entry(process_current(), upage);
		ASSERT(entry != NULL);

		/* If page is present, write data back to file. */
		if ((kpage = pagedir_get_page(t->pagedir, upage)) != NULL &&
				pagedir_is_dirty(t->pagedir, upage)) {

			/* Coarse grained filesystem lock for loading */
			lock_acquire(&filesys_lock);

			/* Fill in executable source content. */
			file_seek(entry->file, entry->ofs);

			if (file_write(entry->file, kpage, entry->size) != (off_t) entry->size) {
				/* We prepared this mapping to exactly fit the file. */
				NOT_REACHED();
			}

			lock_release(&filesys_lock);
			palloc_free_page(kpage);
		}

		enum intr_level old_level;
		old_level = intr_disable();
		bool spte_exists = spt_remove_entry(process_current(), upage);
		ASSERT(spte_exists);

		uninstall_page(upage);
		intr_set_level(old_level);

		if (size <= PGSIZE)
			break;

		size -= PGSIZE;
		upage += PGSIZE;
	}

	return true;
}

/* Adds a mapping from user virtual address UPAGE to kernel
   virtual address KPAGE to the page table.
   If WRITABLE is true, the user process may modify the page;
   otherwise, it is read-only.
   UPAGE must not already be mapped.
   KPAGE should probably be a page obtained from the user pool
   with palloc_get_page().
   Returns true on success, false if UPAGE is already mapped or
   if memory allocation fails. */
bool install_page(void *upage, void *kpage, struct page_flags flags)
{
	ASSERT(is_page_aligned(upage));
	ASSERT(is_page_aligned(kpage));

	struct thread *t = thread_current();

	/* Verify that there's not already a page at that virtual
	   address, then map our page there. */
	if (pagedir_get_page(t->pagedir, upage) != NULL)
		return false;

	return pagedir_set_page(t->pagedir, upage, kpage, flags);
}

/* A wrapper for calling pagedir_release_page with
   the pagedir of the current thread. */
void uninstall_page(void *upage)
{
	ASSERT(is_page_aligned(upage));

	struct thread *t = thread_current();

	pagedir_release_page(t->pagedir, upage);
}
