#ifndef VM_VM_H
#define VM_VM_H

#define MAX_STACK_SIZE 0x800000
#define MAX_STACK_OFFSET 0x20

int handle_user_page_fault(struct thread *t, void *fault_addr, uint8_t *sp);

bool is_stack_access(struct thread *t, void *fault_addr, uint8_t *sp);

uint8_t *lazy_load_file_page(struct process *p, void *upage);

int handle_stack_growth(uint8_t *upage);

int handle_lazy_loading(uint8_t *upage, struct thread *t, void *fault_addr);

bool is_free_segment(void *vaddr, size_t size);

bool prepare_segment(struct file *file, off_t ofs, uint8_t *upage,
		uint32_t read_bytes, uint32_t zero_bytes, bool writable);

bool free_segment(void *upage, size_t size);

bool install_page(void *upage, void *kpage, struct page_flags flags);

void uninstall_page(void *upage);

#endif				/* vm/vm.h */
