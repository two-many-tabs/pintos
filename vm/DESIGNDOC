			+---------------------------+
		    |         ProgOS		    |
		    | PROJECT 2: VIRTUAL MEMORY	|
		    |	   DESIGN DOCUMENT	    |
		    +---------------------------+

---- GROUP ----

>> Fill in the names and email addresses of your group members.

Christian Leopoldseder <christian.leopoldseder@student.tuwien.ac.at>
Raphael Eikenberg <raphael.eikenberg@student.tuwien.ac.at>

---- PRELIMINARIES ----

>> If you have any preliminary comments on your submission, notes for the
>> TAs, or extra credit, please give them here.

>> Please cite any offline or online sources you consulted while
>> preparing your submission, other than the Pintos documentation, course
>> text, lecture notes, and course staff.

			PAGE TABLE MANAGEMENT
			=====================
---- DATA STRUCTURES ----

>> A1: Copy here the declaration of each new or changed `struct' or
>> `struct' member, global or static variable, `typedef', or
>> enumeration.  Identify the purpose of each in 25 words or less.

process.h:

struct process {
	...

	/* Owned by spt.c */
	struct hash spt; /* Supplemental page table for file pages */
};


spt.h:

/* Represents an entry in the supplemental page table.
   One entry corresponds to exactly one file page. */
struct spte {
	struct hash_elem hash_elem; /* Used by hash table */
	uint8_t *vaddr;             /* Virtual address of the entry. (is page aligned) */
	struct file *file;          /* File to which the page is mapped */
	off_t ofs;                  /* Offset where the page mapping starts in the file */
	size_t size;                /* How far (starting at OFS) the mapping reaches */
};


pte.h:

/* Struct representing meta information for a page. */
struct page_flags {
	bool present; /* Indicating the page is currently mapped. */
	bool writable; /* Indicating if the page is writable or not. */
	uint32_t avl; /* Type of reservation of the page. */
};

---- IMPLEMENTATION ----
>> A2: Describe how you implemented lazy page loading. How do you
>> determine the file and file offset corresponding to a virtual
>> address?

Since the actual page table included three bits that were not being used
anywhere else (AVL), we used those to mark pages as reserved for specific
purposes (stack, file, etc.). This enabled us to save memory since the
additional information that's needed for individual purposes diverge. In fact,
we generalized file mappings and the mapping of the executable source, and
called that type a file page. File pages need additional information such as an
offset in the mapped file and the mapped file itself so that we're able to
retrieve the data when lazily loading the page. This information is stored in a
supplemental page table. If a page fault occures, we first query the page table
to determine the type of the page. For all-zero pages, a blank page is loaded.
For file pages, we have to query the supplemental page table to get the
information as explained above.

---- SYNCHRONIZATION ----
>> A3: How do you deal with or prevent page faults in file system
>> routines. Argue why your solution is deadlock free.

Ther is potential for a deadlock when the filesys_lock is acquired and a page
fault occurs, because if the page fault causes a lazy load of a file the
filesys_lock would be acquired again.  A page fault never occurrs in the
sections where the filesys_lock is acquired, because there are only pages
accessed that are already loaded.


			    STACK GROWTH
			    ============

---- DATA STRUCTURES ----

>> B1: Copy here the declaration of each new or changed `struct' or
>> `struct' member, global or static variable, `typedef', or
>> enumeration.  Identify the purpose of each in 25 words or less.

thread.h:

struct thread {
	...

	/* Owned by exception.c. */
	uint8_t *esp;		/* Stack pointer while executing a system call. */
};


---- IMPLEMENTATION ----
>> B2: Describe how you integrated the (potentially growing) stack segment
>> into your page table management implementation.

As described above, we keep track of a pages purpose in the page table using
the (initially unused) AVL flags. However, we cannot know how many pages are
needed for the stack when the process starts (except for the first stack page).
To overcome this issue, pages are flagged as stack pages using heuristics when
a page fault occures. These are further described in the next question. For
stack pages, no information is stored in the supplemental page table as all
information needed is already provided by the actual page table.

---- ALGORITHMS ----
>> B3: Explain how you detect a page fault which should trigger a
>> stack growth. What asssumptions are needed for your implementation
>> to work?

To determine if a page should be a stack page, we implemented a simple
heuristic which checks the distance of the faulted address to the current
stack pointer of the running thread. The maximum distance was chosen to allow
for the PUSHA instruction to function correctly (32 bytes). Also, the maximum
stack size was implemented to be compatible with the default on most Unix
systems (8 MB).

			 MEMORY MAPPED FILES
			 ===================

---- DATA STRUCTURES ----

>> C1: Copy here the declaration of each new or changed `struct' or
>> `struct' member, global or static variable, `typedef', or
>> enumeration.  Identify the purpose of each in 25 words or less.

process.h:

/* In the current implementation, the capacity is fixed to 512 (PGSIZE/sizeof(mmap_mapping)) */
struct mmap_table {
	struct mmap_mapping *mappings;
	int mmap_free;		/* lowest-index free mmap table entry */
	int mmap_max;		/* highest-index used mmap table entry */
	int mmap_cap;		/* mmap table capacity */
};

/* Represents a file mapping */
struct mmap_mapping {
	void *start_addr; /* The virtual start address of the mapping */
	size_t size;      /* The size of the mapping */
};

struct process {
	...

	/* Memory mappings */
	struct mmap_table mmap_table; /* File descriptor table */
};

---- ALGORITHMS ----

>> C2: Describe how memory mapped files integrate into your virtual memory
>> subsystem.

As explained, memory mapped file and process executable pages are generalized
as 'file pages'. When a memory mapping is created, the corresponding entries in
the supplemental page table are created with it. By setting the AVL bits in the
page table, the pages become reserved. On a page fault these pages are lazily
loaded like the pages of the executable file.

>> C3: Explain how you determine whether a new file mapping overlaps
>> any existing segment.

Before reserving a page for the mapping the function is_free_segment() is
called to check for overlaps. It starts at the given start address and from
there iteratively checks if the following pages that are needed to accommodate
a mapping of the given size are not yet reserved.

---- RATIONALE ----

>> C4: Mappings created with "mmap" have similar semantics to those of
>> data demand-paged from executables, except that "mmap" mappings are
>> written back to their original files, not to swap.  This implies
>> that much of their implementation can be shared.  Explain why your
>> implementation either does or does not share much of the code for
>> the two situations.

The same function is used to reserve pages in the two cases since the
reservation works equally thanks to our generalization. The code to free the
pages is different, as the mmap pages are written back to the files.

			   SURVEY QUESTIONS
			   ================

Answering these questions is optional, but it will help us improve the
course in future quarters.  Feel free to tell us anything you
want--these questions are just to spur your thoughts.  You may also
choose to respond anonymously in the course evaluations at the end of
the quarter.

>> In your opinion, was this assignment, or any one of the three problems
>> in it, too easy or too hard?  Did it take too long or too little time?

>> Did you find that working on a particular part of the assignment gave
>> you greater insight into some aspect of OS design?

>> Is there some particular fact or hint we should give students in
>> future quarters to help them solve the problems?  Conversely, did you
>> find any of our guidance to be misleading?

>> Do you have any suggestions for the TAs to more effectively assist
>> students, either for future quarters or the remaining projects?

>> Any other comments?
